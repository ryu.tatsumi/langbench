#include <stdio.h>
#include <stdlib.h>

#define BUF_MAX 1024

void make_sum(char *str, int *p_exp, int *p_sum, int *p_flag)
{
    int exp = 0;
    int num = 0;
    int sum = 0;
    while (1) {
        if (*str == ',') {
            str++;
            break;
        }
        else if (*str >= '0' && *str <= '9') {
            exp = exp*10 + (*str-'0');
        }
        else {
            printf("WARNING: invalid character: %c, treated as 0\n", *str);
            exp = exp*10;
        }
        str++;
    }
    while (1) {
        if (*str == ',' || *str == '\0' || *str == '\n') {
            sum += num;
            num = 0;
            if (*str == '\0' || *str == '\n') {
                break;
            }
        }
        else if (*str >= '0' && *str <= '9') {
            num = num*10 + (*str-'0');
        }
        else {
            printf("WARNING: invalid character: %c, treated as 0\n", *str);
            num = num*10;
        }
        str++;
    }

    *p_exp = exp;
    *p_sum = sum;
    *p_flag = (sum == exp) ? 0 : 1;
}

void process(char *filename_in, char *filename_out)
{
    FILE *fin, *fout;
    char buf[BUF_MAX];
    fin = fopen(filename_in, "r");
    fout = fopen(filename_out, "w");
    while (fgets(buf, BUF_MAX, fin) != NULL) {
        int exp, sum, flag;
        make_sum(buf, &exp, &sum, &flag);
        fprintf(fout, "%d,%d,%d\n", exp, sum, (exp == sum ? 0 : 1));
    }
    fclose(fin);
    fclose(fout);
}

int main(int argc, char **argv)
{
    process("in", "out-c");
}
