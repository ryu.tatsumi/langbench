
sub sum {
  my $ret = 0;
  foreach my $num (@_) {
    $ret += $num;
  }
  return $ret;
}

sub process {
  my ($filename_in, $filename_out) = @_;
  open (IN, "$filename_in");
  open (OUT, "> $filename_out");

  while(<IN>) {
    chomp;
    my @num_list = split(/,/);
    my $exp = shift(@num_list);
    my $sum = &sum(@num_list);
    my $flag = ($exp == $sum) ? 0 : 1;
    print OUT "$exp,$sum,$flag\n";
  }

  close(IN);
  close(OUT);
}

&process("in", "out-pl");
