
let rec make_sum lst cur exp = 
  match lst with
  | [] -> if cur = exp then (cur, 0) else (cur, 1)
  | hd::tl -> make_sum tl (cur+int_of_string(hd)) exp

let process filename_in filename_out = 
  let ic = open_in filename_in in
  let oc = open_out filename_out in
  try
    while true do
      let strs = String.split_on_char ',' (input_line ic) in
      let exp = int_of_string (List.hd strs) in
      let lst = List.tl strs in
      let (cur, flag) = make_sum lst 0 exp in
      Printf.fprintf oc "%d,%d,%d\n" exp cur flag
    done
  with End_of_file -> () ;
  close_in ic;
  close_out oc

let () = 
  process "in" "out-ml"
