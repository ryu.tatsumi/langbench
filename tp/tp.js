
function process(filename_in, filename_out) {
  const fs = require("fs");
  const readline = require("readline");
  const rs = fs.createReadStream(filename_in);
  const ws = fs.createWriteStream(filename_out);
  const reader = readline.createInterface({ input: rs });
  reader.on("line", (data) => {
    const strs = data.split(",");
    const exp = strs[0];
    const sum = strs.slice(1)
        .map(x => parseInt(x, 10))
        .reduce((acc, cur) => acc+cur);
    const flag = (exp == sum) ? 0 : 1;
    ws.write(`${exp},${sum},${flag}\n`);
  });
}

process("in", "out-js");
