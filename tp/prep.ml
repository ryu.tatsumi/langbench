
let rec random_list len =
  if len = 0 then [] else (Random.int 100000000)::random_list (len-1)

let rec sum lst =
  match lst with
  | [] -> 0
  | hd::tl -> hd + sum tl

let prepare filename = 
  let oc = open_out filename in
  Random.self_init() ;
  for i = 0 to 1000000 do
    let num_list = random_list 10 in
    let num_exp = sum num_list in
    Printf.fprintf oc "%d,%s\n" num_exp (String.concat "," (List.map string_of_int num_list))
  done;
  close_out oc

let () = 
  prepare "in"
