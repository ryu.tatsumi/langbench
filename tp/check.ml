
exception Wrong_Calculation

let check filename = 
  let ic = open_in filename in
  try
    while true do
      let strs = String.split_on_char ',' (input_line ic) in
      let flag = List.nth strs 2 in
      if flag <> "0" then raise Wrong_Calculation
    done
  with End_of_file -> Printf.printf "CHECK OK: %s\n" filename
     | Wrong_Calculation -> Printf.printf "CHECK ERROR: %s\n" filename;
  close_in ic

let () = 
  if Array.length Sys.argv > 1 then
    check Sys.argv.(1)
  else
    check "out-ml"
