
import java.io.*;

public class tp {
    public void process(String filename_in, String filename_out) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(filename_in));
            PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(filename_out)));
            String data;
            while ((data = br.readLine()) != null) {
                String[] strs = data.split(",", -1);
                int exp = Integer.parseInt(strs[0]);
                int sum = 0;
                for (int i = 1; i < strs.length; i++) {
                    sum += Integer.parseInt(strs[i]);
                }
                pw.format("%1$d,%2$d,%3$d\n", exp, sum, (sum == exp ? 0 : 1));
            }
            br.close();
            pw.close();
        } catch (IOException e) {
            System.out.println("ERROR: " + e.getMessage());
        }
    }
    public static void main(String[] args) {
        tp tp = new tp();
        tp.process("in", "out-java");
    }
}
