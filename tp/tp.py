#!/usr/bin/python3

def sum_str(lst):
    ret = 0
    for str in lst:
        ret += int(str)
    return ret

def process(filename_in, filename_out):
    with open(filename_in, 'r') as fin, open(filename_out, 'w') as fout:
        for line in fin:
            strs = line.split(",")
            exp = int(strs[0])
            total = sum_str(strs[1:])
            fout.write("%d,%d,%d\n" % (exp, total, 0 if (total == exp) else 1))

if __name__ == "__main__":
    process("in", "out-py")
