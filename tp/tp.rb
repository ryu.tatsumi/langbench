
def process(filename_in, filename_out)
  File.open(filename_in, mode = "rt"){|fin|
    File.open(filename_out, mode = "wt"){|fout|
      fin.each_line{|line|
        strs = line.split(",", -1);
        exp = strs[0].to_i
        nums = strs[1..-1]
        sum = 0
        for num in nums
          sum += num.to_i;
        end
        fout.printf("%d,%d,%d\n", exp, sum, (exp == sum ? 0 : 1))
      }
    }
  }
end

process("in", "out-rb")
