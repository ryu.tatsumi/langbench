# 概要

プログラミング言語ごとのパフォーマンスの違いを測定するため、
同等と考えられる処理を複数のプログラミング言語で実装し、
実行時間を測定する。

各プログラミング言語ごとの固有の最適化は行っていない（行う能力もない）ので、
プログラミング言語の特性を把えればもっと高速化出来る余地はあるかもしれない。

# ベンチマーク内容

実行できるベンチマークは以下の2種類。
いずれも、処理内容はソースコードを参照のこと。

1. フィボナッチ関数(fib)

  fib(40)を求める。

2. テキスト処理(tp)

  CSV形式のファイルを読み込み、各行に記載の数値の合計を求める。

# 実行方法

```
cd fib
make run-all
cd ../tp
make run-all
```

run-allの代わりにrun-XXX (XXXはプログラミング言語に対応する文字列）をターゲットにすればそのプログラミング言語に関してだけ実行できる。文字列はMakefileを参照。

# ベンチマーク結果のサンプル

* 環境

```
[user@user-PC langbench]$ lscpu
アーキテクチャ: x86_64
CPU op-mode(s):        32-bit, 64-bit
Byte Order:            Little Endian
CPU(s):                4
On-line CPU(s) list:   0-3
コアあたりのスレッド数:2
ソケットあたりのコア数:2
Socket(s):             1
NUMAノード:         1
ベンダーID:        GenuineIntel
CPUファミリー:    6
モデル:             37
Model name:            Intel(R) Core(TM) i7 CPU       U 620  @ 1.07GHz
ステッピング:    2
CPU MHz:               1067.000
CPU max MHz:           1067.0000
CPU min MHz:           666.0000
BogoMIPS:              2128.02
仮想化:             VT-x
L1d キャッシュ:   32K
L1i キャッシュ:   32K
L2 キャッシュ:    256K
L3 キャッシュ:    4096K
NUMAノード 0 CPU:   0-3
Flags:                 fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx rdtscp lm constant_tsc arch_perfmon pebs bts nopl xtopology nonstop_tsc aperfmperf eagerfpu pni pclmulqdq dtes64 monitor ds_cpl vmx smx est tm2 ssse3 cx16 xtpr pdcm sse4_1 sse4_2 popcnt aes lahf_lm ssbd ibrs ibpb stibp tpr_shadow vnmi flexpriority ept vpid dtherm ida arat spec_ctrl intel_stibp flush_l1d
[user@user-PC langbench]$ uname -a
Linux user-PC 3.10.0-1127.el7.x86_64 #1 SMP Tue Feb 18 16:39:12 EST 2020 x86_64 x86_64 x86_64 GNU/Linux
[user@user-PC langbench]$ cat /etc/redhat-release
Red Hat Enterprise Linux Server release 7.8 (Maipo)
[user@user-PC langbench]$ 
```

* 結果

| 言語 | 結果(fib) | 結果(tp) |
| --- | --- | --- |
|C|real 1.86<br>user 1.86<br>sys 0.00|real 1.76<br>user 1.34<br>sys 0.09|
|haskell[interpreter]|real 46.81<br>user 46.12<br>sys 0.78||
|haskell[native]|real 43.77<br>user 43.35<br>sys 0.41||
|java|real 0.87<br>user 0.85<br>sys 0.03|real 6.82<br>user 9.02<br>sys 0.51|
|javascript|real 3.04<br>user 3.03<br>sys 0.01|real 10.53<br>user 8.99<br>sys 0.44|
|ocaml[bytecode]|real 9.42<br>user 9.42<br>sys 0.00|real 11.26<br>user 9.47<br>sys 0.08|
|ocaml[interpreter]|real 9.41<br>user 9.40<br>sys 0.01|real 9.71<br>user 9.45<br>sys 0.09|
|ocaml[native]|real 1.16<br>user 1.16<br>sys 0.00|real 3.54<br>user 2.83<br>sys 0.08|
|perl|real 196.47<br>user 196.43<br>sys 0.00|real 10.80<br>user 8.91<br>sys 0.09|
|python|real 104.82<br>user 104.81<br>sys 0.00|real 11.49<br>user 10.93<br>sys 0.16|
|ruby|real 36.16<br>user 36.15<br>sys 0.01|real 31.22<br>user 30.93<br>sys 0.15|

[result-fib](result-fib)  
[result-tp](result-tp)
