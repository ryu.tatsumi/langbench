#!/bin/bash

result_format() {
  IN=$1
  OUT=$2
  awk -f result-format.awk ${IN} | sort > ${OUT}
}

LEFT=result-fib
RIGHT=result-tp

LEFT_FORMAT=${LEFT}.tmp.$$
RIGHT_FORMAT=${RIGHT}.tmp.$$

result_format ${LEFT} ${LEFT_FORMAT}
result_format ${RIGHT} ${RIGHT_FORMAT}

echo "| 言語 | 結果(fib) | 結果(tp) |"
echo "| --- | --- | --- |"
join -t "	" -a 1  ${LEFT_FORMAT} ${RIGHT_FORMAT} | awk -F "	" '{print "|" $1 "|" $2 "|" $3 "|"}'

rm -f ${LEFT_FORMAT} ${RIGHT_FORMAT}
