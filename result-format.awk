function print_line(lang, real, user, sys) {
  print lang "\t" "real " real "<br>user " user "<br>sys " sys 
}

BEGIN {
  FIRST=1
}
/^-+ Time for ([A-Za-z\[\]]+) -+/ {
  if (FIRST == 0) {
    print_line(LANG, REAL, USER, SYS)
  }
  LANG=$4
  FIRST=0
}
/^real / {
  REAL=$2
}
/^user / {
  USER=$2
}
/^sys / {
  SYS=$2
}
END {
  print_line(LANG, REAL, USER, SYS)
}
