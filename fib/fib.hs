fib :: (Ord a, Num a) => a -> a
fib = \x -> if x <= 1 then x else fib (x-1) + fib (x-2)

main = do
    print $ fib 40
