sub fib {
   my $n = $_[0];
   if ($n <= 1) {
       return $n;
   }
   else {
       return &fib($n-1)+&fib($n-2);
   }
}

print &fib(40)."\n";
